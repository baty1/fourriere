(* Executive summary: simpl does not play well with deep nestings. *)

Definition plus :=
  fun m =>
    fix pl (n: nat) :=
      match n with
      | 0 => m
      | S n' => S (pl n')
      end.

Axiom m : nat.
Lemma test :
  plus m (plus m (plus m (plus m (plus m (plus m (plus m (plus m (
  plus m (plus m (plus m (plus m (plus m (plus m (plus m (plus m (
  plus m (plus m (plus m (plus m (plus m m))))
  ))))))))
  ))))))))
  = 0.
Proof.
  (* It is impossible to simplify this expression; let's try anyways *)
  Time cbn.   (* fast *)
  Time simpl. (* slow: simpl is dumb when it comes to nested functions *)
Abort.
