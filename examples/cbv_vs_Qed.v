(* Executive summary: Qed can run slower than what comes before it. *)
Require Import fourriere.common.

Lemma base: slow = 0.
Proof.
  Time reflexivity.
  Set Printing Implicit.
  Show Proof. (* (@eq_refl nat 0) *)
Time Qed. (* slower *)

Lemma cbv_no_useful_trace: slow = 0.
Proof.
  Time cbv. (* slow *)
  reflexivity.
  Set Printing Implicit.
  Show Proof. (* (@eq_refl nat 0 : slow = 0) *)
  (* Note how the term proof got bigger for seemingly no good reason *)
Time Qed. (* slower *)

Time Definition manual_1: slow = 0 := (@eq_refl nat 0).
Time Definition manual_2: slow = 0 := (@eq_refl nat 0 : slow = 0).
Time Check (@eq_refl nat 0).
Time Check (@eq_refl nat 0 : slow = 0).

(* cbv is more efficient than the reduction method used by Qed in this
   situation. *)
