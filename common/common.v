(* Inefficient computation that returns 0 *)
Fixpoint slow_comp (n: nat) :=
  match n with
  | S n' => slow_comp n' + slow_comp n'
  | 0 => 0
  end.

(* Defines constants which are slow to compute, but not too much so. *)
Definition slow_c    := 20. (* feel free to adapt *)
(* vm_compute is faster, so we give it more stuff to compute *)
Definition slow_vm_c := 24. (* feel free_to adapt *)

Definition slow     := slow_comp slow_c. (* ~0.5s, YMMV *)
Definition slow'    := slow_comp slow_c. (* Same as above (don't tell Coq) *)
Definition slow_vm  := slow_comp slow_vm_c. (* ~0.4s with vm_compute, YMMV *)
Definition slow_vm' := slow_comp slow_vm_c. (* Same as above (don't tell Coq) *)

(* Reduces to slow *)
Fixpoint slow_comp_indirect n :=
  match n with
  | 0 => slow
  | S n' => slow_comp_indirect n'
  end.
Definition slow_indirect := slow_comp_indirect 5.
