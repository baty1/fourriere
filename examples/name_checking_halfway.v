(* Executive summary: Coq's typechecking procedure is weird and the check for
   reused names seems to happen right at the middle of it. *)
Require Import fourriere.common.

Time Definition new_name: slow = 0 := eq_refl. (* slow *)
Set Printing All.
Print new_name.

(* Still take some time, but only about half as much as before (in fact, it
   takes about the time it takes to apply reflexivity to "slow = 0"): *)
Time Fail Definition new_name: slow = 0 := eq_refl.

(* Experiments: *)
(* I expect these to be fast: *)
Time Check (@eq_refl nat slow). (* fast *)
Time Check (@eq_refl nat slow: slow = slow). (* fast *)
(* I expect these to be slow: *)
Time Check (@eq_refl nat slow : slow = 0). (* slow (2 computations) *)
Time Check (@eq_refl nat slow : 0 = slow). (* slow (2 computations) *)
Time Check (@eq_refl nat 0 : slow = slow). (* slow (2 computations) *)
(* I don't know what to expect of these. On the one hand, there's only one
possible reduction, on the other hand Coq is weird. *)
Time Check (@eq_refl nat 0 : slow = 0). (* slow (2 computations) *)
Time Check (@eq_refl nat 0 : 0 = slow). (* slow (2 computations) *)
Time Check (@eq_refl nat slow : 0 = 0). (* slow (2 computations) *)
(* Oh well. *)
(* These should fail half-way: *)
Time Fail Check (@eq_refl nat slow : 1 = 0). (* slow (1 computation) *)
Time Fail
  Check ((@eq_refl nat slow : 0 = 0) : 0 = 1).  (* slow (1 computation) *)
Time Fail
  Check ((@eq_refl nat 0 : 0 = 0) : 1 = slow). (* slow (1 computation) *)
(* I don't know what I am doing at this point. *)
(* Neither does Coq, apparently. *)
Time Check ((@eq_refl nat 0 : 0 = 0) : 0 = slow). (* slow (2 computations) *)
Time Check ((@eq_refl nat slow : 0 = 0) : 0 = slow). (* slow (4 computations) *)
Time
  Check (((@eq_refl nat slow : 0 = 0) : 0 = slow) : slow = 0).
  (* slow (8 computations) *)
Time
  Check (((@eq_refl nat slow : 0 = 0) : 0 = 0) : slow = 0).
  (* slow (4 computations, as expected) *)
Time
  Check ((((@eq_refl nat slow : 0 = 0) : 0 = slow) : slow = 0) : 0 = slow).
  (* slow (but only 12 computations) *)
Time
  Check (
    ((((@eq_refl nat slow : 0 = 0) : 0 = slow) : slow = 0) : 0 = slow)
    : slow = 0
  ). (* slow (16 computations) *)
