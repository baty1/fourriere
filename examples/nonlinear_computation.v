(* Executive summary: computing twice something != computing something twice. *)
Require Import fourriere.common.

Lemma base_measure: slow = 0.
Proof.
  Time reflexivity. (* slow *)
Time Qed. (* slow *)

Lemma five_times_as_slow_are_you_kidding_me: slow = slow'.
Proof.
  Time reflexivity. (* five times as slow *)
Time Qed. (* same *)
