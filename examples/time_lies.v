(* Executive summary: Coq is bad at tracking time. *)
(* Another example of this can be found in forgotten_strategy.v. *)

(* From https://github.com/coq/coq/issues/8236 *)
(* More detail over here: https://coq.zulipchat.com/#narrow/stream/237656-Coq-devs.20.26.20plugin.20devs/topic/.60Time.60.20is.20inaccurate.20since.20Coq.208.2E5/near/235257702 *)

Lemma truth: True.
Proof.
  do 2000 (pose proof I).
  exact I.
Time Qed. (* Almost instantaneous according to Coq but I beg to differ. *)

Lemma truth_2: True.
Proof.
  do 2000 (pose proof I).
  exact I.
  Time Optimize Proof. (* Takes about as long as the previous Qed. *)
Time Qed. (* fast *)
