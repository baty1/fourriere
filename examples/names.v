(* Executive summary: non-breaking spaces in your identifiers? Sure! *)
Lemma Ça joue ツ: 0 = 0.
Proof. reflexivity. Qed.
(* No support for emojis (alas?). *)
