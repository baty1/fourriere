(* Executive summary: doing now x is like doing x followed by easy, which can
   have an impact on performance. *)
Require Import fourriere.common.

Lemma now_test: slow_indirect = slow.
Proof.
  Time reflexivity.
Restart.
  Time easy.
Restart.
  Time now idtac.
Qed.

(* easy is generally slower than reflexivity *)
Lemma easy_vs_refl: slow = slow'.
Proof.
  Time reflexivity. (* slow *)
Restart.
  Time easy. (* slower *)
Qed.
