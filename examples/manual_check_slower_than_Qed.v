(* Executive summary: typechecking with Qed is faster than with Definition. *)
(* Related to name_checking_halfway.v. *)
Require Import fourriere.common.

Lemma base: slow = 0.
Proof.
  Time reflexivity. (* slow: 1 computation *)
  Set Printing All.
  Show Proof.
Time Qed. (* slow: 1 computation *)

Time Definition manual: slow = 0 := @eq_refl nat 0. (* slow: 2 computations *)

(* For the record, this seems to be the fastest way of getting this version of
   the proof through the kernel: *)
Lemma base_skip_checks: slow = 0.
Proof.
  change_no_check (0 = 0).
  reflexivity.
  Set Printing All.
  Show Proof. (* same proof *)
Time Qed. (* slow: 1 computation *)

(* Couldn't manual behave like base_skip_checks? *)
