(* Executive summary: vm_compute memoizes the value of constants. *)
Require Import fourriere.common.

Lemma memoization_of_constants: slow_vm = 0.
Proof.
  Time vm_compute. (* slow *)
  reflexivity.
Time Qed. (* fast (thanks to memoization) *)

Lemma memoization_survives_past_qed: slow_vm + 1 = 1.
Proof.
  Time vm_compute. (* fast *)
  reflexivity.
Time Qed. (* fast *)

Lemma no_memoization_when_no_constant: slow_vm = 0.
Proof.
  (* When we unfold a constant before computing it value, memoization doesn't
     apply. *)
  unfold slow_vm.
  Time vm_compute. (* slow *)
  reflexivity.
Time Qed. (* slow *)

Lemma i_said_no_memoization_when_no_constant: slow_vm = slow_vm.
Proof.
  (* When we unfold a constant before computing it value, memoization doesn't
     apply. *)
  unfold slow_vm.
  Time vm_compute. (* slow (2 computations) *)
  reflexivity.
Time Qed. (* slow (2 computations) *)

(* Using slow_vm' instead of slow_vm here: same definition, but its value has
   not been memoized yet. *)
Lemma memoization_is_smartish: slow_vm' = slow_vm'.
Proof.
  Time vm_compute. (* slow, but not twice as slow: value computed only once! *)
  reflexivity.
Time Qed. (* fast, memoized by now *)

Lemma other_tactics_dont_memoize: slow = 0.
Proof.
  Time reflexivity. (* slow *)
Time Qed. (* also slow *)

(* TODO try native_compute *)
