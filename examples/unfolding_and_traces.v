(* Executive summary: unfold leaves a trace in the proof term except when it
   doesn't. *)
Require Import fourriere.common.

Lemma raw_computation: slow = slow'.
Proof.
  Time reflexivity. (* slow *)
  Set Printing Implicit.
  Show Proof. (* @eq_refl nat slow' *)
Time Qed. (* slow *)

Lemma unfold_leaves_a_trace: slow = slow'.
Proof.
  unfold slow, slow'.
  Time reflexivity. (* fast *)
  Set Printing Implicit.
  Show Proof. (* @eq_refl nat (slow_comp 20) : slow = slow' *)
Time Qed. (* fast *)

Lemma unfold_in_hypothesis_doesnt_leave_a_trace:
  forall x y, x = slow -> y = slow' -> x = y.
Proof.
  intros.
  unfold slow, slow' in *.
  rewrite H, H0.
  reflexivity. (* fast *)
  Set Printing Implicit.
  Show Proof.
  (* (fun (x y : nat) (H : x = slow) (H0 : y = slow') =>
       @eq_ind_r nat
         (slow_comp 20)
         (fun x0 : nat => x0 = y)
         (@eq_ind_r nat
           (slow_comp 20)
           (fun y0 : nat => slow_comp 20 = y0)
           (@eq_refl nat (slow_comp 20))
           y H0)
         x H
       : x = y) *)
Time Qed. (* but slow here :( *)

Lemma same_as_previous_but_unfold_before_intros_therefore_fast:
  forall x y, x = slow -> y = slow' -> x = y.
Proof.
  unfold slow, slow' in *.
  intros.
  rewrite H, H0.
  reflexivity. (* fast *)
  Set Printing Implicit.
  Show Proof.
  (* ((fun (x y : nat) (H : x = slow_comp 20) (H0 : y = slow_comp 20) =>
       @eq_ind_r nat
         (slow_comp 20)
         (fun x0 : nat => x0 = y)
         (@eq_ind_r nat
           (slow_comp 20)
           (fun y0 : nat => slow_comp 20 = y0)
           (@eq_refl nat (slow_comp 20))
           y H0)
         x H)
       : forall x y : nat, x = slow -> y = slow' -> x = y)
       The only part that changes is the type of H and H0 and this last line (as
       well as some parentheses)
       *)
Time Qed. (* fast *)
