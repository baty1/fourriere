(* Executive summary: just because you described a strategy to prove something
   without running into performance issues doesn't mean that Coq will remember
   it by the time Qed is reached. *)
Require Import Arith_base.

Axiom opaque_nat: nat.
Axiom opaque_nat_is_O: opaque_nat = 0.
Fixpoint contrived (n: nat) (acc: nat) : nat :=
  match n with
  | 0 => acc
  | S n' =>
    if (15 <=? n) then contrived n' acc
    else
      contrived n' (if opaque_nat =? 0 then 1 else 2)
      + contrived n' (if opaque_nat =? 0 then 1 else 2)
  end.

Lemma test_contrived: contrived 101 0 = contrived 16 0.
Proof.
  Time simpl. (* slow *)
  Time reflexivity. (* fast *)
Succeed Time Qed. (* fastish *)
Restart.
  (* Don't evaluate contrived when its second argument doesn't start with a
     constructor: *)
  Arguments contrived n !acc.
  Time simpl. (* fastish *)
  Time reflexivity. (* fast *)
Succeed Time Qed. (* slow *)
Restart.
  Time reflexivity. (* slow *)
Time Qed. (* slow *)

Fixpoint contrived_2 (n: nat) (acc: nat) : nat :=
  match n with
  | 0 => acc
  | S n' =>
    if (14 <=? n) then contrived_2 n' acc
    else contrived_2 n' (if opaque_nat =? 0 then 1 else contrived_2 n' acc)
  end.

Lemma test_contrived_2: contrived_2 100 0 = contrived_2 15 0.
Proof.
  Time simpl. (* slow, time lies *)
  Time reflexivity. (* fast *)
Succeed Time Qed. (* fastish *)
Restart.
  Arguments contrived_2 n !acc.
  Time simpl. (* fastish *)
  Time reflexivity. (* fast *)
  Set Printing All.
  Show Proof. (* hmm *)
Succeed Time Qed. (* never got it to terminate *)
Restart.
  Time reflexivity. (* never got it to terminate *)
Time Qed. (* who knows *)
