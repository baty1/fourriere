(* Executive summary: unfold tries to be smart and this can backfire. *)
Require Import fourriere.common.

Lemma frustrating: slow_comp 17 = slow_comp 16 + slow_comp 16.
Proof.
  (* Unfolding once would be enough but `unfold` tries to be helpful. *)
  unfold slow_comp at 1. (* Come on *)
  reflexivity.
Qed.

(* In fact, I couldn't find an easy way of telling Coq "unfold the definition of
   `slow_comp` precisely once" *)
